#include "xFitCalc.h"
#include "LHAPDF/LHAPDF.h"

#include <stdlib.h>
#include <map>
#include <fstream>
#include <sstream>


using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::map;
using std::domain_error;
using std::max;
using std::ofstream;
using std::ifstream;
using std::stringstream;

xFitCalc::xFitCalc(string Collider,
                   string PDFset,
                   bool PDFuncertainty,
                   double ECM,
                   double mtop,
                   double mtoplimit,
                   double mtopstep,
                   bool Resummation,
                   string orderFO,
                   string orderRES,
                   bool LO,
                   bool NLO,
                   bool NNLO,
                   int precision,
                   int npdfGrid,
                   string outfolder)
{

  map<string, vector<string> > options;

  const int NumberParameters = 29;  //Number of params

  string sPDFuncertainty("");
  if(PDFuncertainty)
    sPDFuncertainty = "YES";
  else
    sPDFuncertainty = "NO";


  std::ostringstream osECM;
  osECM << ECM;
  string sECM = osECM.str();

  std::ostringstream osmTop;
  osmTop << mtop;
  string smTop = osmTop.str();


  std::ostringstream osmtoplimit;
  osmtoplimit << mtoplimit;
  string smtoplimit = osmtoplimit.str();


  std::ostringstream osmtopstep;
  osmtopstep << mtopstep;
  string smtopstep = osmtopstep.str();


  string sResummation("");
  if(Resummation)
    sResummation = "YES";
  else
    sResummation = "NO";

  string sLO("");
  if(LO)
    sLO = "YES";
  else
    sLO = "NO";

  string sNLO("");
  if(NLO)
    sNLO = "YES";
  else
    sNLO = "NO";

  string sNNLO("");
  if(NLO)
    sNNLO = "YES";
  else
    sNNLO = "NO";


  std::ostringstream osprecision;
  osprecision << precision;
  string sprecision = osprecision.str();

  std::ostringstream osnpdfGrid;
  osnpdfGrid << npdfGrid;
  string snpdfGrid = osnpdfGrid.str();


  string defaults[NumberParameters*2] = {
    //================================= General setup (Collider, pdf, F.O. vs RES.)
    "Collider", Collider,
    "WithResummation", sResummation,
    "PDFuncertainty", sPDFuncertainty,
    "RestrictedScaleVariation", "2.0",
    "PDFset", PDFset,
    "PDFmember", "0",
    //================================= mt(GeV); muF and muR (in units of mt)
    "Mtop", smTop,
    "MtopLimit", smtoplimit,//unattainable value. Do NOT modify!
    "MtopStep", smtopstep,
    "muF", "0.5 1.0 2.0",
    "muR", "0.5 1.0 2.0",
    //================================= Resummation
    "OrderFO", orderFO,
    "OrderRES", orderRES,
    "A", "0.0",
    "TwoLoopCoulombs", "YES",
    "H2gg1", "53.17",//normalization as/(pi)
    "H2gg8", "96.34",
    "H2qq", "84.81",
    //================================= Fixed Order
    "LO", sLO,
    "NLO", sNLO,
    "NNLO", sNNLO,
    //================================= Setup parameters
    "ECMLHC", sECM,
    "ECMTEV", "1960",
    "Precision", sprecision,
    "NPdfGrid", snpdfGrid,
    "ETA", "1e-5",
    "CMP", "2.7",
    "PdfFileType", "LHgrid",
    "PartonChannel", "ALL"
  };

  for (int i = 0; i < NumberParameters; ++i){
    string key=defaults[2*i];
    string buffer=defaults[2*i+1];
    stringstream line(buffer);
    string val;
    vector<string> values;
    while (line){
      val.clear();
      line >> val;
      if (val.empty())
        continue;
      values.push_back(val);
    }
    options[key] = values;
  }

  _outfolder = outfolder;

  init(options);

}


void xFitCalc::init(map<string, vector<string> > options)
{
  // assignment of the actual options
  _A = atof(options["A"][0].c_str());
  _CMP = atof(options["CMP"][0].c_str());
  _ECMLHC = atof(options["ECMLHC"][0].c_str());
  _ECMTEV = atof(options["ECMTEV"][0].c_str());
  _ETA = atof(options["ETA"][0].c_str());
  _H2gg1 = atof(options["H2gg1"][0].c_str());
  _H2gg8 = atof(options["H2gg8"][0].c_str());
  _H2qq = atof(options["H2qq"][0].c_str());
  _Mtop = atof(options["Mtop"][0].c_str());
  _MtopLimit   = atof(options["MtopLimit"][0].c_str());
  _MtopStep = atof(options["MtopStep"][0].c_str());
  _NPdfGrid = atoi(options["NPdfGrid"][0].c_str());
  _PDFmember = atoi(options["PDFmember"][0].c_str());
  _Precision = atoi(options["Precision"][0].c_str());

  _LO = options["LO"][0];
  _NLO = options["NLO"][0];
  _NNLO = options["NNLO"][0];
  _OrderFO = options["OrderFO"][0];
  _OrderRES = options["OrderRES"][0];
  _PDFuncertainty = options["PDFuncertainty"][0];
  _WithResummation = options["WithResummation"][0];
  _Collider = options["Collider"][0];
  _PDFset = options["PDFset"][0];
  _TwoLoopCoulombs = options["TwoLoopCoulombs"][0];
  _RestrictedScaleVariation = options["RestrictedScaleVariation"][0];
  _PdfFileType = options["PdfFileType"][0];
  _PartonChannel = options["PartonChannel"][0];


  // Check if this option's value is allowed (either "NO" or a "double >= 1.0"):
  // Recall: atof(ptr-to-non-double) == 0.0
  if(_RestrictedScaleVariation != "NO" && atof(_RestrictedScaleVariation.c_str()) < 1.0){
    throw domain_error("Incorrect option RestrictedScaleVariation: (must be either NO or a double >= 1.0).");
  }

  // Set default value _MtopLimit=_Mtop.
  if (_MtopLimit == -1.0){
    _MtopLimit = _Mtop;
  }

  // Guard the range of this option to avoid infinite loops:
  if (_MtopStep <= 0){
    throw domain_error("Incorrect option MtopStep: (must be a positive number).");
  }

  if (_PdfFileType!="LHgrid" && _PdfFileType!="LHpdf"){
    throw domain_error("Unknown type of pdf file (possibilities: LHgrid or LHpdf).");
  }

  if (_PDFuncertainty == "YES"){//For pdf variation, use oly central scales.
    _vmuF.push_back(1.0);
    _vmuR.push_back(1.0);
  }
  else{
    for(vector<string>::iterator mu = options["muR"].begin(); mu != options["muR"].end(); ++mu){
      _vmuR.push_back(atof(mu->c_str()));
    }
    for(vector<string>::iterator mu = options["muF"].begin(); mu != options["muF"].end(); ++mu){
      _vmuF.push_back(atof(mu->c_str()));
    }
    if(_vmuF.size() == 0){
      throw domain_error("Please specify values for muF.");
    }
    if (_vmuR.size() == 0){
      throw domain_error("Please specify values for muR.");
    }
  }


  // When with 'RESUMMATION', set the F.O. parameters according to the RES ones:
  if(_WithResummation=="YES"){
    if(_OrderFO=="LO"){
      _LO =   "YES";
      _NLO =  "NO";
      _NNLO = "NO";
    }
    else if(_OrderFO=="NLO"){
      _LO =   "YES";
      _NLO =  "YES";
      _NNLO = "NO";
    }
    else if(_OrderFO=="NNLO"){
      _LO =   "YES";
      _NLO =  "YES";
      _NNLO = "YES";
    }
    else{
      throw domain_error("Unknown option for Resummation matching (Expected LO, NLO or NNLO)");
    }
  }

  if(_Collider == "TEV"){
    _ECM=_ECMTEV;
  }
  else{
    _ECM=_ECMLHC;
  }


  /*******************************************************************************
  *									                                             *
  *	      Check for consistency and completeness all string                      *
  *             options that are passed as bool to the classes.                  *
  *									                                             *
  *******************************************************************************/
  if(_Collider!="LHC" && _Collider!="TEV"){
    throw domain_error("Unknown type of collider (possibilities: TEV or LHC).");
  }

  if(_LO!="YES" && _LO!="NO"){
    throw domain_error("Unknown option for the LO result (possibilities: YES or NO).");
  }

  if(_NLO!="YES" && _NLO!="NO"){
    throw domain_error("Unknown option for the NLO result (possibilities: YES or NO).");
  }

  if(_NNLO!="YES" && _NNLO!="NO"){
    throw domain_error("Unknown option for the NNLO result (possibilities: YES or NO).");
  }

  if(_TwoLoopCoulombs!="YES" && _TwoLoopCoulombs!="NO"){
    throw domain_error("Unknown option about Two-loop Coulombs (possibilities are: YES or NO).");
  }

  if(_OrderRES!="LL" && _OrderRES!="NLL" && _OrderRES!="NNLL"){
    throw domain_error("Unknown option with resummation order (possibilities: LL, NLL, NNLL).");
  }

  if(_OrderFO!="LO" && _OrderFO!="NLO" && _OrderFO!="NNLO"){
    throw domain_error("Unknown option with resummation matching to F.O. (possibilities: LO, NLO, NNLO).");
  }


  /*******************************************************************************
  *									                                             *
  *                 Setup for the computation of pdf uncertainties               *
  *									                                             *
  *******************************************************************************/

  // ALL prescriptions for comp. PDF uncert's MUST be included in the next 2 lines:
  const int NumPdfPrescr = 4;
  string arrpdf[NumPdfPrescr] = {"Asymmetric","NNPDF","Symmetric","HERA_VAR"};

  vector<string> ListKnownPdfPrescriptions;
  for (int i = 0; i < NumPdfPrescr; ++i){
    ListKnownPdfPrescriptions.push_back( arrpdf[i] );
  }

  // Store all user-defined pairs (pdf set, pdf uncert. prescription)
  map<string, vector<string> > PdfPairs;

  ifstream readpdf("pdf.cfg");
  while(readpdf){
    string buffer;
    getline(readpdf, buffer);
    if(buffer.empty()){
      continue;
    }

    stringstream line(buffer);
    string key, val;
    vector<string> values;
    line >> key;

    while(line){
      val.clear();
      line >> val;
      if(val.empty()){
        continue;
      }
      if (val[0] == '/'){
        break;
      }
      values.push_back(val);
    }
    PdfPairs[key] = values;
  }


  // assignment of the actual option for Computing Pdf Uncertainty
  _PdfUncertMethod = "Unknown";
  if(_PDFuncertainty == "YES"){
    if(PdfPairs.count(_PDFset) > 0){ //pdf set is in the list:
	  bool flag = true;
      if(PdfPairs[_PDFset].size() == 0){
	    flag = false;
        PdfPairs[_PDFset].push_back("Unknown");
        cout<<"PdfPairs[_PDFset].size() = "<<PdfPairs[_PDFset].size()<<endl;
      }
	  if(PdfPairs[_PDFset].size() != 1){
	    cout
        << "\n!!!!!!!!!!!!!!!!!!!!!!!!!   WARNING   !!!!!!!!!!!!!!!!!!!!!!!!!\n"
        " Too many pdf prescriptions for pdf set "
		<< _PDFset
		<< " (must be one).\n"
		" The program will proceed using the prescription \""
		<< PdfPairs[_PDFset][0]
		<< "\".\n"
		<< "!!!!!!!!!!!!!!!!!!!!!!!   END WARNING   !!!!!!!!!!!!!!!!!!!!!!!\n"
		<< endl;
      }

      // check if the method requested by the user is known:
      vector<string>::iterator it = find(ListKnownPdfPrescriptions.begin(),
                                         ListKnownPdfPrescriptions.end(),
                                         PdfPairs[_PDFset][0]);

      if(it == ListKnownPdfPrescriptions.end() && flag != false){ //unknown method
	    cout
		<< "\n!!!!!!!!!!!!!!!!!!!!!!!!!   WARNING   !!!!!!!!!!!!!!!!!!!!!!!!!\n"
		" The prescription "
		<< PdfPairs[_PDFset][0]
		<< " for computing pdf uncertainty is unknown.\n"
		" The program will continue without computing pdf uncertainty.\n"
		" The results for the individual pdf members will be displayed.\n"
		<< "!!!!!!!!!!!!!!!!!!!!!!!   END WARNING   !!!!!!!!!!!!!!!!!!!!!!!\n"
		<< endl;
	  }
      else if(flag == false){ //no method was specified
	    cout
		<< "\n!!!!!!!!!!!!!!!!!!!!!!!!!   WARNING   !!!!!!!!!!!!!!!!!!!!!!!!!\n"
		" No prescription for computing pdf uncertainty with set "
		<< _PDFset
		<< " is specified.\n"
		" The program will continue without computing pdf uncertainty.\n"
		" The results for the individual pdf members will be displayed.\n"
		<< "!!!!!!!!!!!!!!!!!!!!!!!   END WARNING   !!!!!!!!!!!!!!!!!!!!!!!\n"
		<< endl;
      }
      else{ //the method PdfPairs[_PDFset][0] is known
        _PdfUncertMethod = PdfPairs[_PDFset][0];
        cout
		<< "NOTE: Pdf uncertainty will be computed with prescription: "
		<< _PdfUncertMethod
		<< "\n"
		<< endl;
      }
	}
    else { //pdf set not in the list:
      cout
	  << "\n!!!!!!!!!!!!!!!!!!!!!!!!!   WARNING   !!!!!!!!!!!!!!!!!!!!!!!!!\n"
	  " No prescription for computing pdf uncertainty with set "
	  << _PDFset
	  << " is specified.\n"
	  " The program will continue without computing pdf uncertainty.\n"
	  " The results for the individual pdf members will be displayed.\n"
	  << "!!!!!!!!!!!!!!!!!!!!!!!   END WARNING   !!!!!!!!!!!!!!!!!!!!!!!\n"
	  << endl;
    }
  }


  /*******************************************************************************
  *									                                             *
  *       		     Initialize the PDF set			                             *
  *									                                             *
  *******************************************************************************/
  if(_PdfFileType == "LHgrid"){
    initPDFSetByName(_PDFset, LHAPDF::LHGRID);
  }
  else{
    initPDFSetByName(_PDFset, LHAPDF::LHPDF);
  }

}





// -------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------
void xFitCalc::mt_sqrs_scancalc(double Mtop, double MtopLimit, double MtopStep, double sqrts)
{

  std::cout<<"!!!!!!!!!!! xFitCalc::mt_sqrs_scancalc works:"<<std::endl;

  _Mtop = Mtop;
  _MtopLimit = MtopLimit;
  _MtopStep = MtopStep;

  // Check again Mtop, MtopLimit, MtopStep before calculations
  if (_MtopLimit == -1.0){
    _MtopLimit = _Mtop;
  }
  if (_MtopStep <= 0){
    throw domain_error("Incorrect option MtopStep: (must be a positive number).");
  }
  //check user's input for consistency
  if(_MtopLimit < _Mtop){
      _MtopLimit = _Mtop;
      cout<<"\n!!!!!!!!!!!!!!!!!!!!!!!!!   WARNING   !!!!!!!!!!!!!!!!!!!!!!!!!\n"
            " MtopLimit is smaller than Mtop. \n"
            " The program will continue with MtopLimit=Mtop.\n"
          <<"!!!!!!!!!!!!!!!!!!!!!!!   END WARNING   !!!!!!!!!!!!!!!!!!!!!!!\n"<<endl;
  }

  // Set up center-of-mass energy
  if(_Collider == "TEV"){
    _ECM = _ECMTEV;
  }
  else{
    _ECM = sqrts;
  }


  _WillOutputSummary = true; //if true at the end, will output in file summary of each run

  // *********** loop over the top mass mt  ***********
  for(double mt = _Mtop; mt <= _MtopLimit; mt += _MtopStep){
    double Rhad = 4 * pow(mt / _ECM, 2);

    // 1. Initialize the partonic fluxes and alpha_s (for each pdf member or for each muR)
    vector<PartonicFlux> vFlux;
    vector<double> AlphaS;
    initpartonicflux(vFlux, AlphaS, mt, Rhad);

    // 2. Initialize the FixedOrder result
    vector<FixedOrder> vFO;
    initfixedorderresult(AlphaS, vFO, mt, Rhad);

    // 3. Initialize the subtraction flux
    vector<SubtrFlux> vSF;
    initsubtractionflux(vFlux, vSF, Rhad);

    // 4. Initialize the Resummation
    vector<Resummation> vRES;
    initresummation(AlphaS, vSF, vRES, mt);

    // 5. Compute the final result
    double central = -1.0; //an unattainable value for sigma_tot
    vector<double> result; // all values of x-section for fixed mt
    vector<vector<double> > resultPrint;// with pdf uncert  : [mt, pdf member, muF, muR, alpha_s, sigma_tot]
    compfinalres(vFlux, AlphaS, vFO, vSF, vRES, resultPrint, result, mt, Rhad, central);

    // 6. Compute scale/pdf variation for a fixed mtop
    double s0 = central;
    //unattainable values
    double mean = -1.0;
    double smax = -1.0;
    double smin = -1.0;
    scalepdfvarforfixedtop(resultPrint, result, mean, smax, smin);

    std::cout<<"s0 =   "<<s0<<std::endl;
    std::cout<<"mean = "<<mean<<std::endl;
    std::cout<<"smax = "<<smax<<std::endl;
    std::cout<<"smin = "<<smin<<std::endl;

    // 7. Output on the screen for fixed mtop.
    screenoutpforfixmtop(mt, s0, mean, smax, smin);

    // 8. Write output results into text file
    writerestofile();



    // ***********************************************************************************************************

  }//END loop over top mass mt.

}


vector<vector<double> >  xFitCalc::getcentrvalres()
{
  return _resultGlobalSummary;
}

vector<vector<vector<double> > > xFitCalc::getpdfeigv()
{
  return _resultGlobal;
}



double xFitCalc::gettopcentralres(string restype)
{
  double mt = 0;
  double topxseccentr  = 0.;
  double topxsecpdfmin = 0.;
  double topxsecpdfmax = 0.;
  for(vector<vector<double> >::iterator i = _resultGlobalSummary.begin(); i != _resultGlobalSummary.end(); ++i){
    int k = 0;
    for(vector<double>::iterator j = (*i).begin(); j != (*i).end(); ++j){
      if(k == 0)
        mt = *j;
      else if(k == 1)
        topxseccentr = *j;
      else if(k == 2)
        topxsecpdfmin = *j;
      else if(k == 3)
        topxsecpdfmax = *j;
      k++;
    }
  }


  double muF;
  double muR;
  double alphas;
  for(vector<vector<vector<double> > >::iterator i = _resultGlobal.begin(); i != _resultGlobal.end(); ++i){
    for(vector<vector<double> >::iterator j = (*i).begin(); j != (*i).end(); ++j){
      int l = 0;
      for(vector<double>::iterator k = (*j).begin(); k != (*j).end(); ++k){
        if(l == 2){
           muF = *k;
        }
        else if(l == 3){
           muR = *k;
        }
        else if(l == 4){
           alphas = *k;
        }
        l++;
      }
      break;
    }
    break;
  }

  if(restype == "mt"){
    return mt;
  }
  else if(restype == "topxseccentr"){
    return topxseccentr;
  }
  else if(restype == "topxsecpdfmin"){
    return topxsecpdfmin;
  }
  else if(restype == "topxsecpdfmax"){
    return topxsecpdfmax;
  }
  else if(restype == "muF"){
    return muF;
  }
  else if(restype == "muR"){
    return muR;
  }
  else if(restype == "alphas"){
    return alphas;
  }

}


vector<double> xFitCalc::gettoppdfeigv()
{

  vector<double> vtopsigmatoteig;
  for(vector<vector<vector<double> > >::iterator i = _resultGlobal.begin(); i != _resultGlobal.end(); ++i){
    for(vector<vector<double> >::iterator j = (*i).begin(); j != (*i).end(); ++j){
      int l = 0;
      for(vector<double>::iterator k = (*j).begin(); k != (*j).end(); ++k){
        if(l == 5){
           vtopsigmatoteig.push_back( *k );
        }
        l++;
      }
    }
  }

  return vtopsigmatoteig;


}


// Initialize the partonic fluxes and alpha_s (for each pdf member or for each muR)
void xFitCalc::initpartonicflux(vector<PartonicFlux> & vFlux, vector<double> & AlphaS, double mt, double Rhad)
{
  std::cout<<"!!!!!!!!!!!!!!!!!!! xFitCalc::initpartonicflux works: "<<std::endl;
  if(_PDFuncertainty == "YES"){ //all pdf members; a single muF value (=mt)
    cout<< "Initializing fluxes and alpha_s for each pdf member ... \n"<< endl;
    for(int i = 0; i <= LHAPDF::numberPDF(); ++i){
    //for(int i = 0; i <= 1; ++i){
      PartonicFlux flux(_Collider=="LHC" ? 1:0,
                                    i,
                                    Rhad,
                                    1.0, //Upper integration limit
                                    _vmuF[0] * mt,
                                    _NPdfGrid,
                                    _Precision);
      vFlux.push_back( flux );
      AlphaS.push_back( LHAPDF::alphasPDF( _vmuR[0] * mt ) );

      cout << i << " out of " << 1 + LHAPDF::numberPDF()
      //cout << i << " out of " << 1 + 1
      << ": alpha_s(muR=" << _vmuR[0] * mt << ") = "
      << LHAPDF::alphasPDF(_vmuR[0] * mt) << ", alpha_s(mZ=91.1876) = "
      << LHAPDF::alphasPDF(91.1876) << endl;
    }
  }
  else{ // all user's muF values; A single pdf member
    cout<<"Initializing fluxes and alpha_s for pdf member "<<_PDFmember<<" ..."<<endl;
    for(vector<double>::iterator it = _vmuF.begin(); it != _vmuF.end(); ++it){
      PartonicFlux flux(_Collider=="LHC" ? 1:0,
                        _PDFmember,
                        Rhad,
                        1.0,
                        (*it) * mt,
                        _NPdfGrid,
                        _Precision);
      vFlux.push_back( flux );
    }
    cout << "alpha_s(mZ=91.1876)=" << LHAPDF::alphasPDF(91.1876) << endl;
    for(vector<double>::iterator it = _vmuR.begin(); it != _vmuR.end(); ++it){
      AlphaS.push_back( LHAPDF::alphasPDF( (*it) * mt ) );
      cout<<"alpha_s( muR="<<(*it)*mt<<" ) = "<<LHAPDF::alphasPDF((*it)*mt)<< endl;
    }
  }

}


// Initialize the FixedOrder result
void xFitCalc::initfixedorderresult(vector<double> AlphaS, vector<FixedOrder> & vFO, double mt, double Rhad)
{
  std::cout<<"!!!!!!!!!!!!!!!!!!! xFitCalc::initfixedorderresult works: "<<std::endl;

  if(_PDFuncertainty == "YES"){
    for(int i = 0; i <= LHAPDF::numberPDF(); ++i){
    //for(int i = 0; i <= 1; ++i){
      //std::cout<<"AlphaS["<<i<<"] = "<<AlphaS[i]<<" mt = "<<mt<<" _vmuF[0] = "<<_vmuF[0]<<" _vmuR[0] = "<<_vmuR[0]<<std::endl;
	  FixedOrder FO(AlphaS[i],
                    mt,
                    _vmuF[0] * mt,
                    _vmuR[0] * mt,
                    _LO   == "YES" ? 1:0,
                    _NLO  == "YES" ? 1:0,
                    _NNLO == "YES" ? 1:0);
      vFO.push_back( FO );
	}
  }
  else{
    for(int i = 0; i != _vmuF.size(); ++i){
      for(int j = 0; j != _vmuR.size(); ++j){
        FixedOrder FO(AlphaS[j],
                      mt,
                      _vmuF[i] * mt,
                      _vmuR[j] * mt,
                      _LO=="YES" ? 1:0,
                      _NLO=="YES" ? 1:0,
                      _NNLO=="YES" ? 1:0);
        vFO.push_back( FO );
      }
    }
  }


}



// Initialize the subtraction flux
void xFitCalc::initsubtractionflux(vector<PartonicFlux> vFlux, vector<SubtrFlux> & vSF, double Rhad)
{

  std::cout<<"!!!!!!!!!!!!!!!!!!! xFitCalc::initsubtractionflux works: "<<std::endl;

  if(_WithResummation == "YES"){
    for(vector<PartonicFlux>::iterator it = vFlux.begin(); it != vFlux.end(); ++it){
      SubtrFlux SF(*it , Rhad, _ETA);
      vSF.push_back( SF );
	}
  }

}

// Initialize the Resummation
void xFitCalc::initresummation(vector<double> AlphaS, vector<SubtrFlux> vSF, vector<Resummation> & vRES, double mt)
{

  std::cout<<"!!!!!!!!!!!!!!!!!!! xFitCalc::initresummation works: "<<std::endl;

  if(_WithResummation == "YES"){
    if(_PDFuncertainty == "YES"){
      for(int i = 0; i <= LHAPDF::numberPDF(); ++i){
      //for(int i = 0; i <= 1; ++i){
        Resummation RES(AlphaS[i],
                        vSF[i],
                        _Precision,
                        mt,
                        _vmuF[0] * mt,
                        _vmuR[0] * mt,
                        _A,
                        _CMP,
                        IntegerForm(_OrderFO),
				        IntegerForm(_OrderRES),
				        _TwoLoopCoulombs == "YES" ? 1:0,
				        _H2qq,
                        _H2gg1,
                        _H2gg8);
        vRES.push_back( RES );
	  }
	}
    else{
      for(int i=0; i != _vmuF.size(); ++i){
        for(int j=0; j != _vmuR.size(); ++j){
          Resummation RES(AlphaS[j],
                          vSF[i],
                          _Precision,
                          mt,
                          _vmuF[i] * mt,
                          _vmuR[j] * mt,
                          _A,
                          _CMP,
                          IntegerForm(_OrderFO),
                          IntegerForm(_OrderRES),
                          _TwoLoopCoulombs == "YES" ? 1:0,
                          _H2qq,
                          _H2gg1,
                          _H2gg8);
          vRES.push_back( RES );
        }
	  }
	}
  }


}



//Compute the final result
void xFitCalc::compfinalres(vector<PartonicFlux> vFlux, vector<double> AlphaS, vector<FixedOrder> vFO, vector<SubtrFlux> vSF,
                            vector<Resummation> vRES,   vector<vector<double> > & resultPrint,         vector<double> & result,
                            double mt,                  double Rhad,                                   double & central)
{

  std::cout<<"!!!!!!!!!!!!!!!!!!! xFitCalc::compfinalres works: "<<std::endl;

  if(_PDFuncertainty == "YES"){ //compute sigma for all pdf members, and for a single (muF,muR) value
    if(vFO.size() != 1+LHAPDF::numberPDF() || _vmuF.size() != 1 || _vmuR.size() != 1){
    //if(vFO.size() != 1+1 || _vmuF.size() != 1 || _vmuR.size() != 1){
	  throw domain_error("Inconsistent number of (muF,muR) pairs with PDF uncertainty determination."
			             "(should be only one), or inconsistent number of pdf members");
      }
      bool pass = ScalesPass(_RestrictedScaleVariation, _vmuF[0], _vmuR[0]);
      for(int i = 0; i <= LHAPDF::numberPDF(); ++i){
      //for(int i = 0; i <= 1; ++i){
        vector<double> element;
        if(pass){
          element.push_back(mt);
          element.push_back(i);
          element.push_back(_vmuF[0]);
          element.push_back(_vmuR[0]);
          element.push_back(AlphaS[i]);
          if(_WithResummation == "YES"){
            result.push_back(ComputeFinalResultFO(vFlux[i], vFO[i], Rhad, _Precision, _PartonChannel) +
                             ComputeFinalResultRES(vFlux[i], vSF[i], vFO[i], vRES[i], Rhad, _Precision, _PartonChannel));
            element.push_back(result.back());
          }
          else{
            result.push_back(ComputeFinalResultFO(vFlux[i], vFO[i], Rhad, _Precision, _PartonChannel));
            element.push_back(result.back());
          }
          //std::cout<<"i = "<<i<<"result.back() = "<<result.back()<<std::endl;
          if(i == 0){
            central = result.back();
            //std::cout<<"central = "<<central<<std::endl;
          }
          cout<<"m_top="<<mt<<", Pdf member "<<i<<", muF="<<_vmuF[0]<<", muR="<<_vmuR[0]
              <<", sigma_tot="<<result.back()<<" [pb]."<<endl;
          resultPrint.push_back(element);
        }
    }
    if(result.empty()){
      throw domain_error("The pair (muF,muR) does not meet the restricted scale variation criteria."
                         " Please review muF and muR.");
    }

  }
  else{ //compute sigma for the central pdf member, and for all user supplied (muF,muR) values
    for(int i = 0; i != _vmuF.size(); ++i){
      for(int j = 0; j != _vmuR.size(); ++j){
        vector<double> element;
        bool pass = ScalesPass(_RestrictedScaleVariation, _vmuF[i], _vmuR[j]);
        if(pass){
          element.push_back(mt);
          element.push_back(_vmuF[i]);
          element.push_back(_vmuR[j]);
          element.push_back(AlphaS[j]);
          if(_WithResummation == "YES"){
            result.push_back(ComputeFinalResultFO(vFlux[i], vFO[i * _vmuR.size() + j], Rhad, _Precision, _PartonChannel)
                                                  + ComputeFinalResultRES(vFlux[i], vSF[i], vFO[ i * _vmuR.size() + j ],
                                                  vRES[i * _vmuR.size() + j], Rhad, _Precision, _PartonChannel));
            element.push_back(result.back());
          }
          else{
            result.push_back( ComputeFinalResultFO(vFlux[i], vFO[i * _vmuR.size() + j], Rhad, _Precision, _PartonChannel) );
            element.push_back(result.back());
          }
          if(_vmuF[i] == 1.0 && _vmuR[j] == 1.0){
            central = result.back();
          }
          cout<<"m_top="<<mt<<", muF="<<_vmuF[i]<<", muR="<<_vmuR[j]
		      <<", sigma_tot="<<result.back()<<" [pb]."<<endl;
          resultPrint.push_back(element);
        }
      }
    }
    if(result.empty()){
      throw domain_error("Not a single pair (muF,muR) meets the restricted scale-variation criteria."
                         " Please review your muF and muR or the option RestrictedScaleVariation.");
    }
  }


}



// Compute scale/pdf variation for a fixed mtop
void xFitCalc::scalepdfvarforfixedtop(vector<vector<double> > resultPrint, vector<double> result, double & mean, double & smax, double & smin)
{
  std::cout<<"!!!!!!!!!!!!!!!!!!! xFitCalc::scalepdfvarforfixedtop works: "<<std::endl;

  if(_PDFuncertainty == "YES"){
    if(_PdfUncertMethod == "NNPDF"){
      deltaPDFnnpdf(result, mean, smax, smin);
    }
    else if (_PdfUncertMethod == "Symmetric"){
      deltaPDFsymmetric(result, smax, smin);
    }
    else if (_PdfUncertMethod == "HERA_VAR"){
      deltaPDFheraVAR(result, smax, smin);
    }
    else if (_PdfUncertMethod == "Asymmetric"){
      deltaPDFasymmetric(result, smax, smin);
    }
  }
  else{ //scale uncertainty
    smax = *max_element(result.begin(),result.end());
    smin = *min_element(result.begin(),result.end());
  }

  _resultGlobal.push_back(resultPrint);

}



// Output on the screen for fixed mtop.
void xFitCalc::screenoutpforfixmtop(double mt, double s0, double mean, double smax, double smin)
{
  std::cout<<"!!!!!!!!!!!!!!!!!!! xFitCalc::screenoutpforfixmtop works: "<<std::endl;

  bool WillOutput = true;

  if(s0 == -1.0){
    WillOutput = false;
    cout<<"\n!!!!!!!!!!!!!!!!!!!!!!!!!   WARNING   !!!!!!!!!!!!!!!!!!!!!!!!!\n"
  	      "Central value cannot be determined.\n"
  	      "Please include the point muF=muR=mt.\n"
  	    <<"!!!!!!!!!!!!!!!!!!!!!!!   END WARNING   !!!!!!!!!!!!!!!!!!!!!!!\n"<<endl;
  }

  if(_PDFuncertainty == "YES" && _PdfUncertMethod == "Unknown"){
    WillOutput = false;
  }


  if(WillOutput){
    if(_PDFuncertainty == "YES"){
      cout<<"****************  Final result (PDF uncertainty):  ****************\n"<<endl;
    }
    else{
      cout<<"**************** Final result (scale variation):  ****************\n"<<endl;
    }

    if(_PartonChannel=="gg"    || _PartonChannel=="qg" ||
  	   _PartonChannel=="qqbar" || _PartonChannel=="qqbarprime" ||
  	   _PartonChannel=="qq"    || _PartonChannel=="qqprime"){
      cout<<"(Calculation of a single partonic channel: "
          <<_PartonChannel<<" -> ttbar+X)\n"<<endl;
    }
    cout<<"sigma_tot = "<<s0<<" + "<<smax - s0<<" ("<<(smax - s0)/s0*100.0
        <<" \%) - "<<s0 - smin<<" ("<<(s0 - smin)/s0*100.0<< " \%)"<<" [pb].\n";

    if(_PDFuncertainty == "YES" && _PdfUncertMethod == "NNPDF"){
      cout<<"Statistical mean = "<<mean<<" [pb] (Note: this is NNPDF-specific info)\n";
    }

  	cout<<"**********************************************************************\n"<<endl;

    vector<double> summary;// summary of the result; for fixed m_top
    summary.push_back(mt);
    summary.push_back(s0);
    summary.push_back(smin);
    summary.push_back(smax);

    _resultGlobalSummary.push_back(summary);
  }
  else{ //if for any mt WillOutput=0, then no Summary for any mt
    _WillOutputSummary = false;
    if(_PartonChannel=="gg" || _PartonChannel=="qg" ||
       _PartonChannel=="qqbar" || _PartonChannel=="qqbarprime" ||
  	   _PartonChannel=="qq" || _PartonChannel=="qqprime"){
      cout<<"\n>>>>> NOTE: calculation of a single partonic channel: "
  	      <<_PartonChannel<<" -> ttbar+X <<<<<\n"<<endl;
    }
  }
}




// Write output results into text file
void xFitCalc::writerestofile()
{

  std::cout<<"!!!!!!!!!!!!!!!!!!! xFitCalc::writerestofile works: "<<std::endl;

  std::ostringstream osECM;
  osECM << _ECM;
  string sECM = osECM.str();

  std::ostringstream osmTop;
  osmTop << _Mtop;
  string smTop = osmTop.str();

  std::cout<<"_outfolder = "<<_outfolder<<endl;
  string outfilename = _outfolder + "topPP_sqrts" + sECM + "_mt" +
                       smTop + "_" + _PDFset + ".res";

  std::cout<<"!!!!!!!!!!!!!!!!!!! outfilename =  "<<outfilename<<std::endl;
  ofstream file( outfilename.c_str() );

  if(_PartonChannel=="gg" || _PartonChannel=="qg" ||
     _PartonChannel=="qqbar" || _PartonChannel=="qqbarprime" ||
     _PartonChannel=="qq" || _PartonChannel=="qqprime"){
    file<<"#>>>>> Calculation of a single partonic channel: "
	    <<_PartonChannel<<" -> ttbar+X <<<<<"<<endl;
  }

  file<<"#----------------------- Input parameters -----------------------#"<<endl;
  if(_PDFuncertainty == "YES"){
    file<<"# PDF variation for sigma_tot"<<"\n# m_top in the range ("
	    <<_Mtop<<","<<_MtopLimit<<")"<<endl;
    file<<"# Collider: "<<_Collider<<"; c.m. energy "
	    <<_ECM<<" GeV"<<endl;
    if(_WillOutputSummary){
      file<<"# PDF set: "<<_PDFset<<"; alpha_s(PDFmember "<<_PDFmember
          <<", mZ=91.1876)="<<LHAPDF::alphasPDF(91.1876)
          <<"\n# pdf uncertainty prescription: "<<_PdfUncertMethod<<endl;
    }
    else{
      file<<"# PDF set: "<<_PDFset<<"; alpha_s(PDFmember "<<_PDFmember<<", mZ=91.1876)="
          <<LHAPDF::alphasPDF(91.1876)<<endl;
    }
    if(_WithResummation == "YES"){
      file<<"# Resummed calculation: "<<_OrderFO<<" + "<<_OrderRES<< "\n# A="<<_A
          << ", TwoLoopCoulombs="<<_TwoLoopCoulombs<< ", H2gg1="<<_H2gg1<<", H2gg8="
          <<_H2gg8<<", H2qq="<<_H2qq<<endl;
    }
    else{
      file<<"# Fixed Order calculation: LO("<<_LO<<"), NLO("<<_NLO<< "), NNLO("
          <<_NNLO<<")"<<endl;
	}
    file<<"# Precision="<<_Precision<<"; NPdfGrid="<<_NPdfGrid<<"; ETA="<<_ETA
        <<"; CMP="<<_CMP<<endl;

    if(_WillOutputSummary){
      file<<"\n#---------------------- Summary of the calculation ----------------------#\n"
          <<"# mt   central   pdf_min   pdf_max"<<endl;
      for(vector<vector<double> >::iterator imt = _resultGlobalSummary.begin(); imt != _resultGlobalSummary.end(); ++imt){
        for(vector<double>::iterator r = (*imt).begin(); r != (*imt).end(); ++r){
          file <<  *r << string(2,' ');
        }
        file<<endl;
      }
    }

    file<<"\n#---------------- Detailed results from the calculation -----------------#\n"
	      "# mt   pdf member   muF   muR   alpha_s   sigma_tot"<<endl;
    for(vector<vector<vector<double> > >::iterator a = _resultGlobal.begin(); a != _resultGlobal.end(); ++a){
      for(vector<vector<double> >::iterator b = (*a).begin(); b != (*a).end(); ++b){
        for(vector<double>::iterator c = (*b).begin(); c != (*b).end(); ++c){
          file <<  *c << string(8,' ');
        }
        file << endl;
      }
    }
  }
  else{
    if(_RestrictedScaleVariation == "NO"){
      file<<"# Unrestricted scale variation for sigma_tot.\n"
            "# m_top in the range ("<<_Mtop<<","<<_MtopLimit<<")."<<endl;
    }
    else{
      file<<"# Restricted scale variation for sigma_tot with RestrictedScaleVariation = "
          <<_RestrictedScaleVariation<< ".\n # m_top in the range ("<<_Mtop<<","<<_MtopLimit<< ")."<<endl;
    }
    file<<"# Collider: "<<_Collider<<"; c.m. energy "<<_ECM<<" GeV"<<endl;
    file<<"# PDF set: "<<_PDFset<<"; PDF member: "<<_PDFmember<<"; alpha_s(mZ=91.1876)="
        <<LHAPDF::alphasPDF(91.1876)<<endl;
    if(_WithResummation == "YES"){
      file<<"# Resummed calculation: "<<_OrderFO<<" + "<<_OrderRES<<"\n# A="<<_A<<", TwoLoopCoulombs="
          <<_TwoLoopCoulombs<<", H2gg1="<<_H2gg1<<", H2gg8="<<_H2gg8<<", H2qq="<<_H2qq<<endl;
    }
    else{
      file<<"# Fixed Order calculation: LO("<<_LO<<"), NLO("<<_NLO<<"), NNLO("<<_NNLO<<")"<<endl;
    }
    file<<"# Precision="<<_Precision<<"; NPdfGrid="<<_NPdfGrid<<"; ETA="<<_ETA<<"; CMP="<<_CMP<<endl;

    if(_WillOutputSummary){
	  file<<"\n#---------------------- Summary of the calculation ----------------------#\n"
            "# mt   central   scale_min   scale_max"<<endl;
      for(vector<vector<double> >::iterator imt = _resultGlobalSummary.begin(); imt != _resultGlobalSummary.end(); ++imt){
        for(vector<double>::iterator r = (*imt).begin(); r != (*imt).end(); ++r){
          file <<  *r << string(2,' ');
        }
        file << endl;
      }
    }

    file<<"\n#---------------- Detailed results from the calculation -----------------#\n"
          "# mt      muF      muR      alpha_s      sigma_tot"<<endl;
    for(vector<vector<vector<double> > >::iterator a = _resultGlobal.begin(); a != _resultGlobal.end(); ++a){
      for(vector<vector<double> >::iterator b = (*a).begin(); b != (*a).end(); ++b){
        for(vector<double>::iterator c = (*b).begin(); c != (*b).end(); ++c){
          file <<  *c << string(8,' ');
        }
        file << endl;
      }
    }
  }

}
