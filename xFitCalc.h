#ifndef guard_class_xFitCalc_h
#define guard_class_xFitCalc_h


/*
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>

using std::string;
using std::vector;
using std::map;
*/



#include <map>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <time.h>
#include <complex>
#include "LHAPDF/LHAPDF.h"
#include <gsl/gsl_errno.h>

#include "PartonicFlux.h"
#include "SubtrFlux.h"
#include "FixedOrder.h"
#include "Resummation.h"
#include "Utilities.h"
#include "psin.h"
#include "lgamma.h"

using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::map;
using std::domain_error;
using std::max;
using std::ofstream;
using std::ifstream;
using std::stringstream;

typedef std::complex<double> dcomp;



class xFitCalc {

public:

  xFitCalc();
  xFitCalc(string Collider,
           string PDFset,
           bool PDFuncertainty,
           double ECM,
           double mtop,
           double mtoplimit,
           double mtopstep,
           bool Resummation,
           string orderFO,
           string orderRES,
           bool LO,
           bool NLO,
           bool NNLO,
           int precision,
           int npdfGrid,
           string outfolder);

  void init(map<string, vector<string> > options);

  void mt_sqrs_scancalc(double Mtop, double MtopLimit, double MtopStep, double sqrts);

  double gettopcentralres(string restype);
  vector<double> gettoppdfeigv();

  vector<vector<double> > getcentrvalres();
  vector<vector<vector<double> > > getpdfeigv();




private:

  double _A;
  double _CMP;
  double _ECMLHC;
  double _ECMTEV;
  double _ECM;
  double _ETA;
  double _H2gg1;
  double _H2gg8;
  double _H2qq;
  double _Mtop;
  double _MtopLimit;
  double _MtopStep;
  int _NPdfGrid;
  int _PDFmember;
  int _Precision;

  string _LO;
  string _NLO;
  string _NNLO;
  string _OrderFO;
  string _OrderRES;
  string _PDFuncertainty;
  string _WithResummation;
  string _Collider;
  string _PDFset;
  string _TwoLoopCoulombs;
  string _RestrictedScaleVariation;
  string _PdfFileType;
  string _PartonChannel;
  string _PdfUncertMethod;
  string _outfolder;

  bool _WillOutputSummary;

  vector<double> _vmuF;
  vector<double> _vmuR;

  vector<vector<vector<double> > > _resultGlobal; // contains pdf variations
  vector<vector<double> > _resultGlobalSummary;   // contains central value of sigma and  pdf_min, pdf_max


  void initpartonicflux(vector<PartonicFlux> & vFlux, vector<double> & AlphaS, double mt, double Rhad);
  void initfixedorderresult(vector<double> AlphaS, vector<FixedOrder> & vFO, double mt, double Rhad);
  void initsubtractionflux(vector<PartonicFlux> vFlux, vector<SubtrFlux> & vSF, double Rhad);
  void initresummation(vector<double> AlphaS, vector<SubtrFlux> vSF, vector<Resummation> & vRES, double mt);

  void compfinalres(vector<PartonicFlux> vFlux, vector<double> AlphaS, vector<FixedOrder> vFO, vector<SubtrFlux> vSF,
                    vector<Resummation> vRES,   vector<vector<double> > & resultPrint,         vector<double> & result,
                    double mt,                  double Rhad,                                   double & central);


  void scalepdfvarforfixedtop(vector<vector<double> > resultPrint, vector<double> result, double & mean, double & smax, double & smin);
  void screenoutpforfixmtop(double mt, double s0, double mean, double smax, double smin);
  void writerestofile();


};

#endif
